//
//  Quiz.m
//  Kwaio
//
//  Created by Saran Onuan on 9/9/2558 BE.
//  Copyright (c) 2558 Sittipong Suwannatrai. All rights reserved.
//

#import "Quiz.h"

@implementation Quiz {
    NSNumber *quizId;
    NSArray *questions;
    NSString *quizName;
    NSString *catalog;
    NSString *imagePath;
    NSString *description;
    NSNumber *qc;
}

- (id)init {
    self = [super init];
    if (self) {
        [self loadData];
    }
    return self;
}

- (void) loadData {
    quizName = @"Wonderful Place";
    qc = @0;
    questions = @[
                  @[@"Thailand.jpg", @"Thailand", @[@"Thailand", @"Combodia", @"Myanmar", @"Laos"]],
                  @[@"TheGreatWall-China.jpg", @"China", @[@"Korea", @"Japan", @"China", @"Russia"]],
                  @[@"Tibet.jpg", @"Tibet", @[@"Tibet", @"Thailand", @"India", @"Mongolia"]],
                  @[@"Tajmahal-India.jpg", @"India", @[@"Persia", @"Bangladesh", @"India", @"Pakistan"]],
                  @[@"Yosemite-USA.jpg", @"USA", @[@"Brazil", @"China", @"Russia", @"USA"]]
                  ];
}

- (void)nextQuestion {
    if ([qc integerValue] + 1 < [self numberOfQuestions]) {
        qc = @([qc intValue] + 1);
    }
}

- (NSString *)getQuizName {
    return quizName;
}

- (NSNumber *)questionCounter {
    return @([qc intValue] + 1);
}

- (NSUInteger)numberOfQuestions {
    return [questions count];
}

- (NSString *)currentImageQuestion {
    return questions[[qc intValue]][0];
}

- (NSString *)currentAnswer {
    return questions[[qc intValue]][1];
}

- (NSArray *)currentChoice {
    return questions[[qc intValue]][2];
}

@end
