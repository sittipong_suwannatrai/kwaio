//
//  Quiz.h
//  Kwaio
//
//  Created by Saran Onuan on 9/9/2558 BE.
//  Copyright (c) 2558 Sittipong Suwannatrai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Quiz : NSObject

- (NSString *)getQuizName;
- (NSNumber *)questionCounter;
- (NSUInteger)numberOfQuestions;
- (NSString *)currentImageQuestion;
- (NSString *)currentAnswer;
- (NSArray *)currentChoice;
- (void)nextQuestion;

@end
