//
//  ResultViewController.m
//  Kwaio
//
//  Created by Saran Onuan on 9/9/2558 BE.
//  Copyright (c) 2558 Sittipong Suwannatrai. All rights reserved.
//

#import "QuizViewController.h"
#import "ResultViewController.h"

@interface ResultViewController ()
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@end

@implementation ResultViewController {
    NSNumber *score;
    NSNumber *maximumScore;
    NSUserDefaults *userDefaults;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initializeData {
    userDefaults = [NSUserDefaults standardUserDefaults];
    if([userDefaults objectForKey:@"score"]!=nil && [userDefaults objectForKey:@"maximumScore"]!=nil) {
        score = [userDefaults objectForKey:@"score"];
        maximumScore = [userDefaults objectForKey:@"maximumScore"];
    } else {
        score = @0;
        maximumScore = @1;
    }
    
    [self updateLabel];
}

- (void)updateLabel {
    NSString *dialog;
    double percentageScore = [score floatValue]/[maximumScore floatValue] * 100;
    if (percentageScore <= 30) {
        dialog = @"Keep Up";
    } else if (percentageScore <= 50) {
        dialog = @"Not Bad";
    } else if (percentageScore <= 70) {
        dialog = @"Cool";
    } else if (percentageScore < 100) {
        dialog = @"Good";
    } else {
        dialog = @"Excellent!";
    }

    self.levelLabel.text = dialog;
    self.scoreLabel.text = [score stringValue];
}

@end
