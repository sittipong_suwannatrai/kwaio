//
//  AppDelegate.h
//  Kwaio
//
//  Created by Saran Onuan on 9/8/2558 BE.
//  Copyright (c) 2558 Sittipong Suwannatrai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

